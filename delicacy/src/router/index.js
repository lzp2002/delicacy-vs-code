import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    redirect: '/Login'
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Register.vue')
  },
  {
    path: '/head',
    name: 'Head',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Head.vue'),
    children: [{
      path: '/pageFirst',
      name: 'PageFirst',
      component: () => import(/* webpackChunkName: "about" */ '../views/PageFirst.vue')
    },
    {
      path: '/detail/:id',
      name: 'Detail',
      component: () => import(/* webpackChunkName: "about" */ '../views/Detail.vue')
    },
    {
      path: '/recipe',
      name: 'Recipe',
      component: () => import(/* webpackChunkName: "about" */ '../views/Recipe.vue')
    },
    {
      path: '/create',
      name: 'Create',
      component: () => import(/* webpackChunkName: "about" */ '../views/Create.vue')
    },
    {
      path: '/space',
      name: 'Space',
      component: () => import(/* webpackChunkName: "about" */ '../views/Space.vue')
    },
    {
      path: '/space/:id',
      name: 'Space',
      component: () => import(/* webpackChunkName: "about" */ '../views/Space.vue')
    }
      ,
    {
      path: '/edit',
      name: 'Edit',
      component: () => import(/* webpackChunkName: "about" */ '../views/Edit.vue')
    }]
  },
]

const router = new VueRouter({
  routes
})

export default router

